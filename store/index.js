// [] Introduce state machines & vuex modules
import {
  ghostAPI,
  postsPerPage,
  postIndexFields
} from '../utilities/endpoints/ghost.js'
import data from '../utilities/endpoints/gapi.js'
// : The state is the JavaScript object that contains the actual data that your app needs to function
export const state = () => ({
  toggleSwitches: {
    cover_on: true,
    nav_load: false,
    nav_on: false,
    modal_on: false,
    active_tab: false
  },
  ghostContent: {
    // : loading data
    posts: null,
    pages: null,
    siteSettings: null,
    siteTags: null,
    featureImg: null
  },
  // reference: {
  //   datawrap: null
  // },
  gsData: {
    calendar: null
  }
})

// : Mutations to the state should only be called by separate functions called actions.
export const mutations = {
  // : Controls/ Switches
  TOGGLE_COVER(state) {
    state.toggleSwitches.cover_on = !state.toggleSwitches.cover_on
  },
  // : Navigation
  CLOSE_NAV: (state) => {
    state.toggleSwitches.nav_on = false
  },
  TOGGLE_NAV: (state) => {
    state.toggleSwitches.nav_on = !state.toggleSwitches.nav_on
  },
  // : Tabs
  SET_ACTIVE_TAB: (state) => {
    state.toggleSwitches.active_tab = true
  },
  // : Move to actions
  SET_INACTIVE_TAB: (state) => {
    state.toggleSwitches.active_tab = false
  },
  TOGGLE_TABS: (state) => {
    state.toggleSwitches.active_tab = !state.toggleSwitches.active_tab
  },
  // : Modals
  SET_MODAL: (state) => {
    state.toggleSwitches.modal_on = false
  },
  OPEN_MODAL: (state) => {
    state.toggleSwitches.modal_on = true
  },
  TOGGLE_MODAL: (state) => {
    state.toggleSwitches.modal_on = !state.toggleSwitches.modal_on
  },
  // : Ghost (Content)
  setFeature(state, featureImg) {
    state.ghostContent.featureImg = featureImg
  },
  setPosts(state, posts) {
    state.ghostContent.posts = posts
  },
  setPages(state, pages) {
    state.ghostContent.pages = pages
  },
  setSiteSettings(state, siteSettings) {
    state.ghostContent.siteSettings = siteSettings
  },
  setSiteTags(state, siteTags) {
    state.ghostContent.siteTags = siteTags
  },
  // : Calendar
  setCalendar(state, calendar) {
    state.gsData.calendar = calendar
  }
}

export const actions = {
  // : commit this mutation
  coverSwitch: (context) => {
    context.commit('TOGGLE_COVER')
  },
  navSwitchClose: (context) => {
    context.commit('CLOSE_NAV')
  },
  navSwitch: (context) => {
    context.commit('TOGGLE_NAV')
  },
  modalSwitch: (context) => {
    context.commit('TOGGLE_MODAL')
  },
  modalClose: (context) => {
    context.commit('SET_MODAL')
  },
  modalOpen: (context) => {
    context.commit('OPEN_MODAL')
  },
  activateTab: (context) => {
    context.commit('SET_ACTIVE_TAB')
  },
  deactivateTab: (context) => {
    context.commit('SET_INACTIVE_TAB')
  },
  tabSwitch: (context) => {
    context.commit('TOGGLE_TABS')
  },
  // : on load
  async nuxtServerInit({ commit, error }) {
    // : Automatically Called
    try {
      // : await Calendar Data
      const calendar = await data
      // : await Ghost Content
      const settings = await ghostAPI().settings.browse()
      const tags = await ghostAPI().tags.browse({ limit: 'all' })
      const posts = await ghostAPI().posts.browse({
        limit: 'all',
        include: 'tags',
        fields:
          'id, slug, published_at, feature_image, featured, title, excerpt, html, primary_tag, page, markdown'
      })
      const pages = await ghostAPI().pages.browse({
        limit: 'all',
        include: 'tags',
        fields:
          'id, slug, feature_image, title, custom_excerpt, meta_title, html, meta_description, url, page, published_at, updated_at'
      })
      // : Feature Images for the slider
      const pageFeatureImg = await ghostAPI().pages.browse({
        limit: 'all',
        fields: 'feature_image'
      })
      commit('setSiteSettings', settings)
      commit('setSiteTags', tags)
      commit('setPosts', posts)
      commit('setPages', pages)
      commit('setFeature', pageFeatureImg)
      commit('setCalendar', calendar)
    } catch (err) {
      // : Since this is server init, the error would be a server error
      console.log({ message: err.message })
    }
  },
  async getIndexPosts({ commit }, pagination) {
    // : Set desired fields for index lists (and tags/authors indices)
    const posts = await ghostAPI().posts.browse({
      limit: postsPerPage,
      page: pagination.pageNumber,
      include: 'tags,authors',
      fields: postIndexFields,
      filter: pagination.filter
    })
    commit('setIndexPosts', posts)
  }
}

// : The getters are just functions that get the data from the state for you.
export const getters = {
  coverState: (state) => state.toggleSwitches.cover_on,
  navState: (state) => state.toggleSwitches.nav_on,
  modalState: (state) => state.toggleSwitches.modal_on,
  activeState: (state) => state.toggleSwitches.active_tab,
  postContent: (state) => state.ghostContent.posts,
  pageContent: (state) => state.ghostContent.pages,
  settings: (state) => state.ghostContent.siteSettings,
  scheduleContent: (state) => state.gsData.calendar
}
